package com.groundgurus.day2.hello;

public class Greet {
  public String greetMe(String name) {
    return "Hello " + name;
  }
}
