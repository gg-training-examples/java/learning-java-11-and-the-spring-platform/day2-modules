package com.groundgurus.day2.main;

import com.groundgurus.day2.hello.Greet;

public class Main {
  public static void main(String[] args) {
    Greet greet = new Greet();
    System.out.println(greet.greetMe("Duke"));
  }
}
